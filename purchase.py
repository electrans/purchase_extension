# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from trytond.model import fields
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    parent = fields.Many2One('purchase.purchase', 'Parent')

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        # set readonly to the header fields when the purchase have a parent
        header_fields = ['party', 'invoice_address', 'warehouse']
        for field in header_fields:
            getattr(cls, field, None).states['readonly'] |=  Eval('parent')


class PurchaseExtension(Wizard):
    """Purchase Extension"""
    __name__ = 'purchase.extension'

    start_state = 'purchase_extension'
    purchase_extension = StateAction('purchase.act_purchase_form')

    def do_purchase_extension(self, action):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        purchases = []
        for id in Transaction().context['active_ids']:
            purchase = Purchase(id)
            if purchase.parent:
                raise UserError(gettext('electrans_pruchase_extension.have_parent'))
            if purchase.state != 'processing':
                raise UserError(gettext('electrans_pruchase_extension.state_processing'))
            p, = Purchase.copy([purchase], default={'lines': []})
            childs = Purchase.search([('parent', '=', purchase.id)])
            if childs:
                p.number = purchase.number + ' (ampl.' + str(len(childs)+1) + ')'
            else:
                p.number = purchase.number + ' (ampl.1)'
            p.parent = purchase.id
            purchases.append(p)
        Purchase.save(purchases)

        if len(purchases) == 1:
            action['views'].reverse()
        return action, {
            'model': 'purchase.purchase',
            'res_id': [purchase.id for purchase in purchases],
            }
        return 'end'
