# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import purchase


def register():
    Pool.register(
        purchase.Purchase,
        module='electrans_purchase_extension', type_='model')
    Pool.register(
        purchase.PurchaseExtension,
        module='electrans_purchase_extension', type_='wizard')
